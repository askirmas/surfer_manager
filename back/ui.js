export {
  buildUI
}

function buildUI({tabs, onAdd}) {
  const table = tagging('table',
    Array.from(tabs).map(([_, tab]) => tab2row(tab))
  );
  document.body.appendChild(table);
  onAdd.subscribe(id => 
    table.appendChild(tab2row(tabs.get(id)))
  )
}


function tab2row(tab) {
    const winTN = texting(tab.windowId.getValue());
    const indexTN = texting(tab.index.getValue());
    const urlTN = texting(tab.url.getValue());
    const icon = tagging('img');
    icon.width = 16;
    icon.height = 16;
    icon.setAttribute('src', tab.favIconUrl.getValue() || '');
    
    tab.windowId.subscribe(windowId => winTN.textContent = windowId);
    tab.index.subscribe(index => indexTN.textContent = index);
    tab.url.subscribe(urlValue => urlTN.textContent = urlValue);
    tab.favIconUrl.subscribe(favIconUrlValue => icon.setAttribute('src', favIconUrlValue || ''));
    
    const [winTd, indexTd, urlTd, iconTd] = [winTN, indexTN, urlTN, icon].map(tn => tagging('td', [tn]));
    winTd.onclick = windowFocusing(tab.windowId);
    urlTd.draggable = true;
    
    const tabTr = tagging('tr', [winTd, indexTd, iconTd, urlTd]);
    tab.closing.subscribe(closing => !closing ? {} : tabTr.parentElement.removeChild(tabTr));

    return tabTr; 
}

function windowFocusing(windowIding) {
  return () => {
    chrome.windows.update(
    windowIding.getValue(),
    {focused: true},
    () => chrome.tabs.getCurrent(tab =>
      chrome.windows.get(
        tab.windowId,
        {populate: true},
        window => {
          if (window.tabs.length === 1) chrome.windows.remove(window.id)
          else chrome.tabs.remove(tab.id)
        }
      )
    )
  )}
}

// not jquery yet
function tagging (tag, els = []) {
  const element = document.createElement(tag);
  els.forEach(el => element.appendChild(el));
  return element
};
function texting (text) {
  return document.createTextNode(text)
}
