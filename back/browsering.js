import '../lib/Rx.js';
export {
  createTabsRegistry,
}

function newTabRx(tab, tabFields){
  return tabFields.reduce(
    (tabRx, key) => {
      tabRx[key] = new Rx.BehaviorSubject(tab[key]);
      return tabRx
    },
    {
      closing: new Rx.Subject
    }
  )
}

function createTabsRegistry(tabFields) {
  return new Promise(resolve => chrome.tabs.query({}, resolve))
  .then(tabs => {
      const tabRegistry = new Map();
      const onAdd = new Rx.Subject;
      tabs.forEach(tab =>
        tabRegistry.set(tab.id, newTabRx(tab, tabFields))
      );
      chrome.tabs.onCreated.addListener(tab => {
        tabRegistry.set(tab.id, newTabRx(tab, tabFields));
        onAdd.next(tab.id)
      });
      
      chrome.tabs.onUpdated.addListener(
        (tabId, args) => {
          if (typeof args === 'object')
            for (let key in args)
              if (tabRegistry.has(tabId)) {
                const tab = tabRegistry.get(tabId),
                  value = args[key]
                ;
                (value === undefined) || (! key in tab)
                ? {}
                : tab[key].next(value)  
              }
        }
      );
      chrome.tabs.onRemoved.addListener(tabId => {
        if (tabRegistry.has(tabId))
          tabRegistry.get(tabId).closing.next(true);
          tabRegistry.delete(tabId);
      });
      return {tabs: tabRegistry, onAdd}
    })
}
