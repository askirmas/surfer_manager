import {buildUI} from './ui.js';
import {createTabsRegistry} from './browsering.js';

document.addEventListener("DOMContentLoaded", main);

function main() {
  return createTabsRegistry(
    ['windowId', 'index', 'status', 'favIconUrl', 'title', 'url']
  )
  .then(buildUI)
}

