chrome.browserAction.onClicked.addListener(() => activateBackPage());

function activateBackPage() {
  const backUrl = chrome.extension.getURL('back.html'); 
  chrome.tabs.query(
    {currentWindow: true, url: backUrl},
    tabs => {
      if (tabs.length === 0)
        chrome.tabs.create({url: backUrl})
      else
        chrome.tabs.update(tabs[0].id, {active: true})
    }
  );
  //chrome.windows.create({url: backUrl, focused: true, type: 'panel'};
}
